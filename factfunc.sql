create or replace function fact (s in number)
return number is
fact int(5):=1;
BEGIN
for i in reverse 1..s
loop
	fact := fact*i;
end loop;
return fact;
END;
