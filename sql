SQL> SELECT * FROM STUDENT_61917;

       RNO NAME 		     MARK1	MARK2	   MARK3
---------- -------------------- ---------- ---------- ----------
	 1 AAKASH			90	   80	      70
	 2 ABHINAV			95	   75	      70
	 3 ABHISHEK			70	   75	      80
	 4 ADARSH			65	   65	      65

SQL> CREATE TABLE EMP_61917(EMP_ID VARCHAR(4), EMP_NAME VARCHAR(10), EMP_SAL NUMBER(6));

Table created.

SQL> CREATE TABLE EMP_RECORDS_61917(EMP_ID VARCHAR(4), EMP_NAME VARCHAR(10));

Table created.

SQL> CREATE TABLE EMP_INC_61917(EMP_ID VARCHAR(4), EMP_SAL_INC NUMBER(6));

Table created.

SQL> CREATE TABLE ACC_61917(ACC_NO VARCHAR(5), ACC_BALANCE(6));
CREATE TABLE ACC_61917(ACC_NO VARCHAR(5), ACC_BALANCE(6))
                                                     *
ERROR at line 1:
ORA-00902: invalid datatype


SQL> CREATE TABLE ACC_61917(ACC_NO VARCHAR(5), ACC_BALANCE VARCHAR(6));

Table created.

SQL> EXIT
Disconnected from Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, OLAP, Advanced Analytics and Real Application Testing options
ccf@FISATPC0360:~$ cd ALAN
ccf@FISATPC0360:~/ALAN$ ls
armstrong.sql  evenodd2.sql  evenodd.sql    factfunc.sql   factproc.sql  pali.sql      salfunc.sql   salproc.sql       studentfunc.sql  sumtable.sql
bill.sql       evenodd3.sql  factfuncp.sql  factprocp.sql  fact.sql      salfuncp.sql  salprocp.sql  studentfuncp.sql  sum.sql
ccf@FISATPC0360:~/ALAN$ sqlplus ugs/fisat@172.16.1.11/fisatug

SQL*Plus: Release 19.0.0.0.0 - Production on Mon Nov 25 14:42:03 2019
Version 19.3.0.0.0

Copyright (c) 1982, 2019, Oracle.  All rights reserved.

Last Successful login time: Mon Nov 25 2019 14:41:39 +05:30

Connected to:
Oracle Database 12c Enterprise Edition Release 12.1.0.2.0 - 64bit Production
With the Partitioning, OLAP, Advanced Analytics and Real Application Testing options

SQL> INSERT INTO EMP_61917 values('E001', 'Susanth', 45000);

1 row created.

SQL> set autocommit on;
SQL> set linesize 300;
SQL> INSERT INTO EMP_61917 values('E002', 'Adarsh', 45000);

1 row created.

Commit complete.
SQL> INSERT INTO EMP_61917 values('E003', 'Ajit', 50000);

1 row created.

Commit complete.
SQL> INSERT INTO EMP_61917 values('E004', 'Francis', 56000);

1 row created.

Commit complete.
SQL> INSERT INTO EMP_61917 values('E005', 'Xavier', 54000);

1 row created.

Commit complete.
SQL> SELECT * FROM EMP_61917;

EMP_ EMP_NAME	   EMP_SAL
---- ---------- ----------
E001 Susanth	     45000
E002 Adarsh	     45000
E003 Ajit	     50000
E004 Francis	     56000
E005 Xavier	     54000

SQL> @afttrig.sql           
 14  /
create or replace trigger empsal after update/delete on EMP_61917 for each row
                                             *
ERROR at line 1:
ORA-00969: missing ON keyword


SQL> @afttrig.sql
 14  /

Trigger created.

SQL> UPDATE EMP_61917 SET EMP_SAL = EMP_SAL + (EMP_SAL/10) WHERE EMP_ID = E005;
UPDATE EMP_61917 SET EMP_SAL = EMP_SAL + (EMP_SAL/10) WHERE EMP_ID = E005
                                                                     *
ERROR at line 1:
ORA-00904: "E005": invalid identifier


SQL> UPDATE EMP_61917 SET EMP_SAL = EMP_SAL + (EMP_SAL/10) WHERE EMP_ID = 'E005';

1 row updated.

Commit complete.
SQL> SELECT * EMP_INC_61917;
SELECT * EMP_INC_61917
         *
ERROR at line 1:
ORA-00923: FROM keyword not found where expected


SQL> SELECT * FROM EMP_INC_61917;

EMP_ EMP_SAL_INC
---- -----------
E005	    5400

SQL> SELECT * FROM ACC_61917;  

no rows selected

SQL> INSERT INTO ACC_61917 values("001", 2000);
INSERT INTO ACC_61917 values("001", 2000)
                             *
ERROR at line 1:
ORA-00984: column not allowed here


SQL> INSERT INTO ACC_61917 values('001', 2000);

1 row created.

Commit complete.
SQL> INSERT INTO ACC_61917 values('002', 3000);

1 row created.

Commit complete.
SQL> INSERT INTO ACC_61917 values('003', 4000);

1 row created.

Commit complete.
SQL> select * from acc_61917;

ACC_N ACC_BA
----- ------
001   2000
002   3000
003   4000

SQL> @beftrig.sql
 10  /

Warning: Trigger created with compilation errors.

SQL> @beftrig.sql
 10  /

Warning: Trigger created with compilation errors.

SQL> @beftrig.sql
 10  /

Trigger created.

SQL> @beftrig.sql               
 10  /

Trigger created.

SQL> UPDATE ACC_61917 SET ACC_BALANCE = 1900 WHERE ACC_BALANCE = 2000;
UPDATE ACC_61917 SET ACC_BALANCE = 1900 WHERE ACC_BALANCE = 2000
       *
ERROR at line 1:
ORA-20001: Low account balance!
ORA-06512: at "UGS.BANKBAL", line 4
ORA-04088: error during execution of trigger 'UGS.BANKBAL'


SQL> @beftrig.sql
 10  /

Trigger created.

SQL> UPDATE ACC_61917 SET ACC_BALANCE = 1900 WHERE ACC_BALANCE = 2000;
UPDATE ACC_61917 SET ACC_BALANCE = 1900 WHERE ACC_BALANCE = 2000
       *
ERROR at line 1:
ORA-20001: Low account balance!
ORA-06512: at "UGS.BANKBAL", line 4
ORA-04088: error during execution of trigger 'UGS.BANKBAL'



