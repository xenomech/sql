DECLARE
i int(2):=1;

BEGIN

loop
	if mod(i,2)=0
	then
		insert into T2_61917 values(i,'even');
	else
		insert into T2_61917 values(i,'odd');
	end if;
	i:=i+1;
	exit when i>20;
end loop;

END;
