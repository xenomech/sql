set serveroutput on;
DECLARE
n number:=&num;
f number;
BEGIN
facto(n,f);
dbms_output.put_line('Factorial = '||f);
END;
