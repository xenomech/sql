create or replace function graade(m in float)
return varchar is
grade char(10);
BEGIN
if m>=90
then
grade:='O';
elsif m>=85 and m<90
then
grade:='A+';
elsif m>=80 and m<85
then
grade:='A';
elsif m>=70 and m<80
then
grade:='B+';
elsif m>=60 and m<70
then
grade:='B';
elsif m>=50 and m<60
then
grade:='C';
elsif m>=45 and m<50
then
grade:='P';
else
grade:='F';
end if;
return grade;
END;
