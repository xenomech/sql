DECLARE

	rol number;
	m number;
	name varchar(10);
	g varchar(10);
	cursor c1 is select rollno,mark from Students56;


BEGIN

	open c1;
	loop 
		fetch c1 into name,m;
		exit when c1%notfound;
		if(m>=80)
		then
			g:='D';
		elsif(m>=60 and m<80)
		then
			g:='F';
		elsif(m>=40 and m<60)
		then
			g:='T';
		else
			g:='Failed';
		end if;
		insert into Grade56 values (name,g);
	end loop;
	close c1;
END;
