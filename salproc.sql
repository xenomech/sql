create or replace procedure sals(s in number, sal out number) as
BEGIN
if s>20000
then
sal:=s+(s*15/100);
elsif s>10000 and s<=20000
then
sal:=s+(s*10/100);
elsif s<10000
then
sal:=s+(s*5/100);
end if;
END;
