create or replace trigger empsal after update or delete on EMP_61917 for each row

DECLARE

BEGIN
if(deleting)
then
	insert into EMP_RECORDS_61917 values(:OLD.EMP_ID, :OLD.EMP_NAME);
elsif(updating)
then
	insert into EMP_INC_61917 values(:NEW.EMP_ID, :NEW.EMP_SAL - :OLD.EMP_SAL);
end if;
END;
