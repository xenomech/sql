set serveroutput on;
DECLARE
roll number;
m1 number;
m2 number;
m3 number;
m4 float;
g char(10);
cursor c1 is select rno, mark1, mark2, mark3 from student_61917;
BEGIN
open c1;
loop
fetch c1 into roll, m1, m2, m3;
exit when c1%notfound;
m4:=(m1+m2+m3)/3;
g:=graade(m4);
dbms_output.put_line(roll||' '||g);
end loop;
close c1;
END;
