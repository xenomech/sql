DECLARE
n number:=&num;
f number;
BEGIN
f:=fact(n);
dbms_output.put_line('Factorial = '||f);
END;
