create or replace trigger bankbal before update on ACC_61917 for each row

BEGIN
if(:NEW.ACC_BALANCE<2000)
then
	raise_application_error(-20001, 'Low account balance!');
end if;

END;
