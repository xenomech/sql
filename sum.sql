DECLARE
a number(5):=&a;
b number(5):=&b;
c number(5);

BEGIN
a:=10;
b:=10;

if a<b
then
c:=a+b;
else
c:=a-b;
endif;

dbms_output.put_line('Sum = '||c);

END;
