set serveroutput on;
DECLARE
	a number:=&a;
	b number;
	c number;
BEGIN
	b:=1;
	c:=1;
	LOOP
		IF(b<=a)
		then 
		c:=c*b;
		END IF;
	b:=b+1;
	EXIT WHEN(b=a+1);
	END LOOP;
dbms_output.put_line(c);
END;
