create or replace procedure facto(s in number, f out number) as
f1 number:=1;
BEGIN
	for i in reverse 1..s
	loop
		f1:= f1*i;
	end loop;
	f:=f1;
END;
