DECLARE

a number(3):=&a;
arm number(5):=0;
i number(3):=a;

BEGIN

while i>0
loop
	arm := arm + (mod(i,10))**3;
	i := trunc(i/10);
end loop;

if a=arm
then
	dbms_output.put_line('Armstrong Number');
else
	dbms_output.put_line('Not Armstrong Number');
end if;

END;
