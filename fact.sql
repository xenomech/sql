DECLARE

a int(3):=&a;
fact int(5):=1;

BEGIN

for i in reverse 1..a
loop
	fact := fact*i;
end loop;

dbms_output.put_line('Factorial = '||fact);

END;
