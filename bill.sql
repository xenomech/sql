set serveroutput on;
DECLARE
cons number;
cur number;
prev number;
unit number;
amounts number;
cursor c1 is select cons_no, prs_read, prev_read from bill_gokul;
BEGIN
open c1;
loop
	fetch c1 into cons, cur, prev;
	exit when c1%notfound;
	unit:=cur-prev;
	if(unit<50)
	then
		amounts:=unit*5;
	elsif(unit>=50 and unit<100)
	then
		amounts:=250+(unit-50)*10;
	elsif(unit>=100 and unit<200)
	then
		amounts:=750+(unit-100)*15;
	elsif(unit>=200)
	then
		amounts:=2250+(unit-200)*20;
	end if;
	update bill_gokul set units=unit, amount=amounts where cons_no=cons;
end loop;
close c1;
END;
