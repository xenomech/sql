create or replace function sal(s in number)
return number is
sal number;
BEGIN
if s>20000
then
sal:=s+(s*15/100);
elsif s>10000 and s<=20000
then
sal:=s+(s*10/100);
elsif s<10000
then
sal:=s+(s*5/100);
end if;
return sal;
END;
