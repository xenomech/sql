set serveroutput on;
DECLARE
eid number;
salary number;
newsal number;
cursor c1 is select emp_id, salary from employee_61917;
BEGIN
open c1;
loop
fetch c1 into eid, salary;
exit when c1%notfound;
sals(salary, newsal);
update employee_61917 set salary=newsal, update_date=sysdate where emp_id=eid;
end loop;
close c1;
END;
